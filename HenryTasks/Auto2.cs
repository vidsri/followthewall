﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HwrBerlin.Bot.Engines;
using HwrBerlin.Bot.Scanner;
using System.Diagnostics;
using System.Globalization;
using HwrBerlin.Bot;
using static HwrBerlin.Bot.Engines.Robot;

namespace HwrBerlin.HenryTasks
{
    class Auto2
    {
        // initializing objects and variables:
        public static Robot _robot = new Robot();
        public static Scanner _scanner = new Scanner();
        
        // threshold is in mm
        public static int safety_threshold = 700;

       

        /// <summary>
        /// This method utilizes the class scanner.cs directly by calling on the methods GetDataList() as well as MedianFilter().
        /// The try-catch block aids in case the received list from those called methods has a lenght of 0, i.e. when the scanner port is blocked.
        /// </summary>
        /// <returns> Returns a list of distances from the scanner. Scanning errors will be corrected via the use of the median correction. </returns>
        public List<int> Scan()
        {
            // initialize local medianList
            var medianList = new List<int>();

            try
            {
                medianList = _scanner.MedianFilter(_scanner.GetDataList());
            }
            catch (Exception e)
            {
                if (e is IndexOutOfRangeException)
                {
                    Debug.WriteLine(e.Message);
                    Debug.WriteLine("length medianList: " + medianList.Count());
                }
            }
            return medianList;
        }

      

        /// <summary>
        /// Getting a list with 181 values for a safe corridor. In this corridor the robot can safely turn around.
        /// </summary>
        /// <returns> Returns a double array list with values in which the robot can safely move. </returns>
        public List<double> generateCorridorList()
        {
            var thresholdlist = new List<double>();
            var reverseThresholdlist = new List<double>();

            // turning radius of the Robot = 44,35 cm
            // adding an extra 10 cm safety distance to the radius on both sides
            // safety_radius = 44,35cm + 10cm + 10cm = 64.35cm -> in MM = 643.5mm
            double safety_radius = 600;

            // initializing the values for the corridor
            // adding the safety_radius itself as value for 0 degrees, as the calculation starts at 1 degree
            thresholdlist.Add(safety_radius);

            // calculation for values in list with index 1-90
            for (int i = 1; i <= 90; i++)
            {
                double threshold = safety_radius / Math.Cos((Math.PI * i / 180.0));
                if (threshold > safety_threshold)
                {
                    threshold = safety_threshold;
                    thresholdlist.Add(threshold);
                }
                else
                    thresholdlist.Add(threshold);
            }

            // adding the value for the safety_threshold to the list for the respective 90 degrees
            thresholdlist.Add(safety_threshold);

            // adding values in list for index 90-179, the values are mirrowed from the first 89 entries in the same list
            for (int i = 179; i >= 90; i--)
            {
                thresholdlist.Add(thresholdlist[i - 89]);
            }
            // adding the safety_radius itself as value for 181 degrees, same as for degree 0, respective position 0 within the list
            thresholdlist.Add(thresholdlist[0]);

            // returning complete list of corridor-distances
            return thresholdlist;
        }

        /// <summary>
        /// Implementation for the real threshold distances (thresholds depending on degree).
        /// This method does a comparison between the scan data (i.e. medianList) and the thresholdlist (see method generateCorridorList()).
        /// </summary>
        /// <returns> Returns the boolean variable drive. True means there is no obstacle in our defined safe corridor. 
       
        public Boolean Check2()
        {
            Debug.WriteLine("entering check2");
            // set drive = false as default for each new entry into this method
            Boolean drive = false;
            Boolean drive1 = false;

            // get medianlist via calling Scan() method after declaration of local list
            List<int> medianList = new List<int>();
            medianList = Scan();
            Debug.WriteLine("medianList ANFANG");
            printArray(medianList);
            Debug.WriteLine("medianList ENDE");

            // get thresholdlist via calling generateCorridorList() Method after the declaration of the local list
            List<double> thresholdlist = new List<double>();
            thresholdlist = generateCorridorList();

            Debug.WriteLine("THRESHOLDLIST ANFANG");
            printArray(thresholdlist);
            Debug.WriteLine("THRESHOLDLIST ENDE");

            // check comparison: compare the values from thresholdlist with the values from the respective degree in the medianlist
            // iterator for thresholdlist starting at index 0
            int i = 0;

            // iterator for medianList starting at index 45 (we ignore the first 45 values, because the robot sees itself in them)
            int j = 45;

            while (i < thresholdlist.Count() - 1 && j < medianList.Count() - 1)
            {
                Debug.WriteLine("current index thresholdlist: " + i);
                Debug.WriteLine("current index medianlist: " + j);
                Debug.WriteLine("current value from medianlist: " + medianList[j]);
                /* Further debugging output if needed, helpful for reverse engineering the decision making process in the code concerning the boolean value for drive
                   Debug.WriteLine("current index thresholdlist: "+ i);
                   Debug.WriteLine("lenght of thresholdlist: " + thresholdlist.Count());
                   Debug.WriteLine("current index medianList: " + j);
                   Debug.WriteLine("length of medianList: " + medianList.Count()); */

                // the Check comparison algorithm to set the boolean drive according to the output of comparison
                if (thresholdlist[i] > medianList[j])
                {
                    // sets stop
                    /* Debug.WriteLine("current index thresholdlist: "+ j);
                       Debug.WriteLine("current index medianlist: "+ i);
                       Debug.WriteLine("current value from medianlist: " + medianList[j]); */

                    Debug.WriteLine("drive == false");
                    drive = false;
                    return drive;
                }
                if (thresholdlist[i] <= medianList[j])
                {
                    Debug.WriteLine("drive == true");
                    drive = true; /*
                    Debug.WriteLine("current index thresholdlist: "+ j);
                    Debug.WriteLine("current index medianlist: "+ i);
                    Debug.WriteLine("current value from medianlist: " + medianList[j]); */
                }
                i++;
                j++;
            }
            if (drive == true)
            {

               drive1 = Check3();
            }
            else drive1 = false;
            // Return the previously set boolean variable drive
            return drive1;
        }


        /// <summary>
        /// Implementation for the real threshold distances (thresholds depending on degree).
        /// This method does a comparison between the scan data (i.e. medianList) and the thresholdlist (see method generateCorridorList()).
        /// we compare 25 values, if the conditions for a wall are met for all 25, then it is considered a wall
        /// </summary>
        /// <returns> Returns the boolean variable drive. True means all 25 values are matched with the wall conditions
        /// and henry is allowed to drive, false means the conditions are not met and henry is not allowed to drive. 
        /// 

        public Boolean Check3()
        {
            Debug.WriteLine("entering check3 ");

            // set drive = false as default for each new entry into this method
            Boolean drive = false;

            // get medianlist via calling Scan() method after declaration of local list
            List<int> medianList = new List<int>();
            medianList = Scan();

            // get thresholdlist via calling generateCorridorList() Method after the declaration of the local list
            List<double> thresholdlist = new List<double>();
            thresholdlist = generateCorridorList();

            // check comparison: compare the values from thresholdlist with the values from the respective degree in the medianlist
        
            // interator for thresholdlist starting at index  154 
            int i = 154;
            //count up to 179 (154+25 =179)

            // iterator for medianList starting at index 200 
            int j = 200;
            //count upto 225 (200+25=225)

            // that proves a consistent wall on the left side of the robot for the 25 values
            while (i < 180 && j < 226)
                {
                Debug.WriteLine("while check3");
               

                // if: actual scan value higher or equal to corridor value AND actual Value not higher than corridor value plus 101
                // wall must be between 60 and 70 cm away from the scaner point 
                
                if (medianList[j] >= thresholdlist[i] && !(medianList[j] > thresholdlist[i] + 101)) 
                { 

                    Debug.WriteLine("current index thresholdlist: " + i);
                    Debug.WriteLine("current index medianlist: " + j);
                    Debug.WriteLine("current value from medianlist: " + medianList[j]);
                    Debug.WriteLine("drive == true");
                    drive = true;
                }
                else
                {
                    Debug.WriteLine("drive == false");
                    Debug.WriteLine("current index thresholdlist: " + i);
                    Debug.WriteLine("current index medianlist: " + j);
                    Debug.WriteLine("current value from medianlist: " + medianList[j]);
                    drive = false;
                    return drive;
                   
                }
                
                i++;
                j++;
            }
            // Return the previously set boolean variable drive to check 2()
            // if it returns here, that means that die robot sees a wall on its left side 
            return drive;
        }




        /// <summary>
        /// Henry drives forward, If there is an obstacle and the conditions from check3() (wall) are met. He stops if there is an 
        /// obsticale or if the wall conditions are not met. 
        /// </summary>


        public void followTheWall()
        {
            int drive_mode = 1;
            int stop_mode = 0;

            _robot.Enable();
            if (_robot != null && _robot.Enable())
            {
                // calling the method Check2() to allocate the boolean value correctly from the scan data
                Boolean drive = Check2();

                // setting the robot into the repsective velocity according to the above input from the method call
                if (drive == false)
                {
                    // sets velocity to zero so that Henry stops
                    _robot.Move(stop_mode);

                }
                else if (drive == true)
                {
                    // sets velocity to 1 so that Henry drives
                    _robot.Move(drive_mode);
                }
            }
        }

       

      

        /// <summary>
        /// Method for printing an array list in the debug command line.
        /// </summary>
        /// <param name="a"> Array that you want to be printed. </param>
        public void printArray<T>(IEnumerable<T> a)
        {
            foreach (var i in a)
            {
                Debug.WriteLine(i);
            }
        }

       

       

       
    }
}
